class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.integer :room_id, :nil => false
      t.string :name
      t.text :description
      t.string :type
      t.boolean :enabled, :default => true

      t.timestamps
    end
    add_index(:images, :name)
    add_index(:images, :room_id)
  end
end
