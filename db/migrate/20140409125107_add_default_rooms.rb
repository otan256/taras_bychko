class AddDefaultRooms < ActiveRecord::Migration
  def change
    Room.create({:name => 'wedding'})
    Room.create({:name => 'kids'})
    Room.create({:name => 'family'})
    Room.create({:name => 'art'})
  end
end
