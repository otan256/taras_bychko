class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.string :name, :nil => false
      t.text :description
      t.boolean :enabled, :default => true

      t.timestamps
    end

    add_index(:rooms, :name)
  end
end
