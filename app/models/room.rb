class Room < ActiveRecord::Base

  has_many :images

  def cover
    images.where({:type => 'cover'}).first
  end

  def self.enabled
    where(:enabled => true)
  end

  def self.valid?(room_name)
    enabled.pluck(:name).include? room_name
  end

end
