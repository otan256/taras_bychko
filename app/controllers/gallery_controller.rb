class GalleryController < ApplicationController

  def index

  end

  def show
    room_name = params[:id]
    redirect_to root_path unless Room.valid?(room_name)
    @room = Room.find_by_name(room_name)
  end

  def create

  end

end
