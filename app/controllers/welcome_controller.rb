class WelcomeController < ApplicationController

  def index
    @rooms = Room.enabled
  end

end
