# Load the Rails application.
require File.expand_path('../application', __FILE__)

GALLERY_ROOMS = %w| wedding kids family art |

# Initialize the Rails application.
TarasBychko::Application.initialize!
